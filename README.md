# README #

개인 프로젝트 
현재는 중단


# 주요 내용 #

- 동영상에 포함된 특정 광고 문구 판단

- WatchDog를 통한 다운로드 디렉토리 감시

- MoviePy를 통한 영상 Clip 

- Tesseract를 통한 문구 판단 후 파일 이동

|샘플 이미지|cv2 사용 이진화|
|-|-|
|![수정됨_picture1.jpg](https://meeta.io:3000/api/image/1523341088862.jpeg)|![수정됨_result.jpg](https://meeta.io:3000/api/image/1523341088860.jpeg)|

- 이진화된 이미지를 Tesseract를 사용하여 판단