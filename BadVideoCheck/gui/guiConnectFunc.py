'''
Created on 2017. 8. 23.

@author: Jhlee
'''
import sys
from PyQt5 import QtCore, QtGui, QtWidgets
import win # @UnresolvedImport
import Test_Packager.all as all  # @UnresolvedImport
from watchDir import MyHandler


class XDialog(QtWidgets.QMainWindow, win.Ui_MainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        # setupUi() 메서드는 화면에 다이얼로그 보여줌
        self.setupUi(self)
 
        self.pushButton.clicked.connect(self.callBtnHandler)
        
    #Flag = True ==> BackGround On
    def callBtnHandler(self):
        if(self.plainTextEdit.toPlainText() != ""):
            dlg.showMinimized()
            all.guiBtnHandle(self.plainTextEdit.toPlainText(), False)
            self.startWatc()
    
    def startWatc(self):
        MyHandler.call_Watch(self, self.plainTextEdit.toPlainText(), False)
#         watch = MyHandler()
#         watch.call_Watch(self.plainTextEdit.toPlainText(), False)
        
app = QtWidgets.QApplication(sys.argv)
dlg = XDialog()
dlg.show()
app.exec_()