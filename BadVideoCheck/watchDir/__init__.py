import time
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer
import Test_Packager.all as all

class MyHandler(FileSystemEventHandler):
    
    def __init__(self, path=""):
        self.path = path if path is "" else path
               
    def on_modified(self, event):
        self.do_action(event)
        
    def do_action(self, event):
        all.guiBtnHandle(self.path, False)
        
    def call_Watch(self, path, Flag=False):
        event_handler = MyHandler(path)
        observer = Observer()
        observer.schedule(event_handler, path, recursive=False)
        observer.start()
        
        if(Flag==False):
            while Flag==False:
                time.sleep(1)
        else:
            observer.stop()
          
        observer.join()  
