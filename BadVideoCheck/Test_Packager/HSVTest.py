import numpy as np  
import cv2  
  
capture = cv2.VideoCapture('/home/Jhlee/Devlopment/workspace/SampleVideo/2.mp4')  
  
# define range of blue color in HSV  
lower_blue = np.array([110,50,50])  
upper_blue = np.array([130,255,255])  
 
lower_red = np.array([0,50,50])  
upper_red = np.array([20,255,255])   
  
while(1):  
  
    ret,frame = capture.read()  
  
    height,width,channel = frame.shape  
  
    img_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)  
  
    # Threshold the HSV image to get only blue colors  
    mask = cv2.inRange(img_hsv, lower_blue, upper_blue)  
#     # Threshold the HSV image to get only red colors  
#     mask = cv2.inRange(img_hsv, lower_red, upper_red)  
  
    numOfLabels, img_label, stats, centroids = cv2.connectedComponentsWithStats(mask)  
  
    # Bitwise-AND mask and original image  
    img_result = cv2.bitwise_and(frame,frame, mask= mask)  
  
  
    for idx, centroid in enumerate(centroids):  
        if stats[idx][0] == 0 and stats[idx][1] == 0:  
            continue  
        if np.any(np.isnan(centroid)):  
            continue  
        x, y, width, height, area = stats[idx]  
        centerX, centerY = int(centroid[0]), int(centroid[1])  
  
        if area > 50 :  
            cv2.circle( frame, (centerX,centerY) ,10, (0,0,255), 10 )  
            cv2.rectangle(frame, (x, y), (x+width, y+height), (0, 0, 255))  
  
    cv2.imshow( 'mask', mask )  
    cv2.imshow( 'img_result', img_result )  
    cv2.imshow( 'webcam', frame )  
  
    if cv2.waitKey(1)&0xFF == ord('q'):  
        break;  
  
capture.release()  
cv2.destroyAllWindows()  