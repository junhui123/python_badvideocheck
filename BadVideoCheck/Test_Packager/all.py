from PIL import Image
from pytesseract import *
import imageio
imageio.plugins.ffmpeg.download()
from moviepy.editor import *
import numpy as np  
import cv2  # @UnresolvedImport
from matplotlib import pyplot as plt
import os
import sys
import shutil #File Move

def OCR(imgData, lang):
    im = Image.open(imgData)
    text = pytesseract.image_to_string(im, lang, config='-psm 6')
    return text

def videoFileSearch(path):
    videoFileList = list()
# 하위 디렉토리 검사 시 무한반복 ... 발생 가능..
#     for (path, dir, files) in os.walk(path):
#         for filename in files:
    for filename in os.listdir(path): 
        ext = os.path.splitext(filename)[-1]
        if ext == '.mp4':
            videoFullPath = os.path.join(path, filename)
            videoFileList.append(videoFullPath)
                    
    return videoFileList

def moveBadVideo(badVideoFilePath):
    pathSplit = badVideoFilePath.split(os.path.sep)
    fileName = pathSplit.pop()
    moveBadVideoDirPath = os.path.join(os.path.sep.join(pathSplit), 'BadVideoTemp')
    moveBadVideoFullPath = os.path.join(os.path.sep.join(pathSplit), 'BadVideoTemp', fileName)
    print(moveBadVideoFullPath)
    if not os.path.isdir(moveBadVideoDirPath):
        os.mkdir(moveBadVideoDirPath)
        
    shutil.move(badVideoFilePath, moveBadVideoFullPath)

def guiBtnHandle(xWinPath, flag=False):
    try:
        if(xWinPath == ""):
            pass
        else:
            start(xWinPath)
    except Exception as e:
        print("exception...") 
        print(e)
    
def start(xWinPath):
    imageFileName = ""
    lang = "kor"
    
    tempL = videoFileSearch(xWinPath)
    for filePath in tempL:
        mv_clip = VideoFileClip(filePath)
        duration = int(mv_clip.duration)
        for i in range(1, 3):
    #         1. image save and read
    #         mv_clip.save_frame("spicture" + str(i) + ".jpg", i)
    #         ret, thresh3 = cv2.threshold(cv2.imread("spicture" + str(i) + ".jpg", 0),127,255,cv2.THRESH_TRUNC)
    #         cv2.imwrite("gspicture" + str(i) + ".jpg", thresh3)
    #         ocrResult = OCR("gspicture" + str(i) + ".jpg", lang)
    #         os.remove("spicture" + str(i) + ".jpg")
    #         os.remove("gspicture" + str(i) + ".jpg")
    
    #         2. image read array
            frame = mv_clip.get_frame(i);
            imageFileName = "temp" +str(i) + ".jpg";
            ret, thresh3 = cv2.threshold(frame,127,255,cv2.THRESH_TRUNC)
            cv2.imwrite(imageFileName, thresh3)
            ocrResult = OCR(imageFileName, lang)
            os.remove(imageFileName)
            print(ocrResult)
            if ocrResult.find('발정') >= 0:
#                 print("")
#                 print('contains true')
#                 print("")
                moveBadVideo(filePath)
                break
