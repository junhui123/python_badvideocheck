import numpy as np  
import cv2  
from matplotlib import pyplot as plt

img_color = cv2.imread( 'picture1.jpg', cv2.IMREAD_COLOR )  
height, width, channel = img_color.shape  
img_gray = np.zeros( (height,width), np.uint8 ) 
  
for y in range(0, height):  
    for x in range(0, width):  
        b = img_color[y,x,0]  
        g = img_color[y,x,1]  
        r = img_color[y,x,2]  
  
        gray = (int(b)+int(g)+int(r))/3.0  
  
        if gray>255:  
            gray=255  
  
        img_gray[y,x]=int(gray)  

# cv2.imshow('Original', cv2.imread( 'picture1.jpg', cv2.IMREAD_GRAYSCALE))
# cv2.waitKey(0)
# cv2.destroyAllWindows()
  
cv2.imwrite('result_g.jpg', cv2.imread( 'picture1.jpg', cv2.IMREAD_GRAYSCALE) )  
#  
# img = cv2.imread('result.jpg',0)
# thresh3 = cv2.threshold(img,127,255,cv2.THRESH_TRUNC) 
# plt.imshow(thresh3,'gray')
# plt.title('TRUNC')
# plt.xticks([]),plt.yticks([])
# plt.show()
