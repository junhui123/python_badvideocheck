import numpy as np  
import cv2   
  
img = cv2.imread("/home/Jhlee/Devlopment/picCrop1.png")
img_hsv=cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

#bgr
green = np.uint8([[[10,10,255 ]]])
hsv_green = cv2.cvtColor(green,cv2.COLOR_BGR2HSV)
print( hsv_green )

# define range of red color in HSV  
lower_red = np.array([0,50,50])
upper_red = np.array([10,255,255])
mask0 = cv2.inRange(img_hsv, lower_red, upper_red)

# upper mask (170-180)
lower_red = np.array([170,50,50])
upper_red = np.array([180,255,255])
mask1 = cv2.inRange(img_hsv, lower_red, upper_red)

mask = mask0+mask1

output_img = img.copy()
output_img[np.where(mask==0)] = 0

output_hsv = img_hsv.copy()
output_hsv[np.where(mask==0)] = 0

cv2.imwrite("/home/Jhlee/Devlopment/img.jpg", output_img)
cv2.imwrite("/home/Jhlee/Devlopment/img_hsv.jpg", output_hsv)

  
cv2.imshow('img', cv2.imread( '/home/Jhlee/Devlopment/img.jpg', 0))
cv2.imshow('img_hsv', cv2.imread( '/home/Jhlee/Devlopment/img_hsv.jpg', 0))
cv2.waitKey(0)
cv2.destroyAllWindows()
