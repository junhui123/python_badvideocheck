'''
Created on 2017. 8. 7.

@author: Jhlee
'''

from PIL import Image
from pytesseract import *


def OCR(imgFile, lang):
    print("")
    print("================================")
    print("Start")
    im = Image.open(imgFile)
    text = pytesseract.image_to_string(im, lang, config='-psm 6')
    
    print("================================")
    print("Result")
    print(text)


if __name__ == '__main__':
#     OCR("Figure_111.png", "eng")
#     OCR("Figure_111.png", "eng+kor")
#     OCR("Figure_111.png", "kor")
    OCR("gspicture1.jpg", "kor")

